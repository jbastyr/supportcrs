<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/', 'PageController@index')->name('index');
Route::get('/website', function() {
    return redirect(config('app.externurl', config('app.url', '/')));
})->name('website');
Route::get('/support', 'PageController@support')->name('support');
//Route::get('/import', 'PageController@importdevices')->name('importdevices');

Auth::routes();

Route::resource('customers', 'CustomerController');

Route::resource('customers/{id}/passwords', 'PasswordController');
Route::resource('customers/{id}/documents', 'DocumentController');
Route::resource('customers/{id}/uploads', 'UploadController');
Route::get('/uploads/{id}/{apikey}', 'UploadController@direct')->name('direct');
Route::resource('customers/{id}/devices', 'DeviceController');
Route::resource('customers/{id}/devices/{devid}/upgradesets', 'UpgradeSetController');
Route::resource('customers/{id}/devices/{devid}/upgradesets/{upsetid}/upgrades', 'UpgradeController');
Route::resource('assets', 'AssetController');
Route::resource('apikeys', 'ApiKeyController');

Route::get('customers/{id}/report', 'CustomerController@report')->name('report');
