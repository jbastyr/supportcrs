@extends('layouts.app')
@section('scripts')
<script>
    function selectCustomer(){
        var customerName = document.getElementById("searchterm").value;
        var selector = 'option[value="' + customerName + '"]';
        var customerID = document.querySelector(selector).id;
        var targetURL = "{{route('customers.index')}}" + "/" + customerID;
        window.location.href = targetURL;
    }
</script>
@endsection
@section('content')
    <h1>
        Customers
        <a href="{{route('customers.create')}}" class="btn btn-primary pull-right">New Customer</a>
    </h1>
    @if(isset($customers) && count($customers) > 0)
        <table class="table table-hover">    
            @foreach($customers as $customer)
                <tr><td>
                    <a href="{{route('customers.show', $customer->id)}}">{{$customer->name}}</a>
                    <a href="{{route('customers.edit', $customer->id)}}" class="pull-right">Edit</a>
                </td></tr>
            @endforeach
        </table>
        {{$customers->links()}}
        <h3>Search</h3>
        <div class="input-group">
            <input type="text" id="searchterm" class="form-control pull-left" list="customers" placeholder="Customer">
            
            <datalist id="customers">
                @if(isset($allCustomers) && count($allCustomers) > 0)
                    @foreach($allCustomers as $customer)
                        <option value="{{$customer->name}}" id="{{$customer->id}}">
                    @endforeach
                @endif
            </datalist>
            <span class="input-group-btn">
                <button class="btn btn-primary pull-right" type="button" id="search" name="search" onclick="selectCustomer();">Select</button>
            </span>
        </div>
    @endif
@endsection