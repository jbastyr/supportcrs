@extends('layouts.app')
@section('content')
    <a href="{{route('customers.index')}}" class="btn btn-default">Go Back</a>
    <h1>
        {{$customer->name}}
    </h1>

    <div class="container">
    <h3>Passwords
    <a href="{{route('passwords.create', $customer->id)}}" class="btn btn-primary pull-right">New Password</a>
    </h3>
    @if(isset($passwords) && count($passwords) > 0)
    <table class="table table-hover">
        <tr>
            <th>Name</th>
            <th>Login</th>
            <th></th>
        </tr>
        @foreach($passwords as $password)
            <tr>
                <td><a href="{{route('passwords.show', [$customer->id, $password->id])}}">{{$password->name}}</a></td>
                <td>{{$password->login}}</td>
                <td><a href="{{route('passwords.edit', [$customer->id, $password->id])}}" class="pull-right">Edit</a></td>
            </tr>
        @endforeach
    </table>
    @endif
    </div>

    <hr>

    <div class="container">
    <h3>Documents
    <a href="{{route('documents.create', $customer->id)}}" class="btn btn-primary pull-right">New Document</a>
    </h3>
    @if(isset($documents) && count($documents) > 0)
    <table class="table table-hover">
        <tr>
            <th>Title</th>
            <th></th>
        </tr>
        @foreach($documents as $document)
            <tr>
                <td><a href="{{route('documents.show', [$customer->id, $document->id])}}">{{$document->title}}</a></td>
                <td><a href="{{route('documents.edit', [$customer->id, $document->id])}}" class="pull-right">Edit</a></td>
            </tr>
        @endforeach
    </table>
    @endif
    </div>
    
    <hr>

    <div class="container">
    <h3>Uploads
    <a href="{{route('uploads.create', $customer->id)}}" class="btn btn-primary pull-right">New Upload</a>
    </h3>
    @if(isset($uploads) && count($uploads) > 0)
    <table class="table table-hover">
        <tr>
            <th>Upload Name</th>
            <th></th>
        </tr>
        @foreach($uploads as $upload)
            <tr>
                <td><a href="{{route('uploads.show', [$customer->id, $upload->id])}}">{{$upload->display_name}}</a></td>
                <td><a href="{{route('uploads.edit', [$customer->id, $upload->id])}}" class="pull-right">Edit</a></td>
            </tr>
        @endforeach
    </table>
    @endif
    </div>

    <hr>

    <div class="container">
    <h3>Devices
    <a href="{{route('devices.create', $customer->id)}}" class="btn btn-primary pull-right">New Device</a>
    </h3>
    @if(isset($devices) && count($devices) > 0)
    <table class="table table-hover">
        <tr>
            <th>Device name</th>
            <th>Has Upgrades</th>
            <th><a href="{{route('report', $customer->id)}}" class="btn btn-info pull-right">Upgrade Report</a></th>
        </tr>
        @foreach($devices as $device)
            <tr>
                <td><a href="{{route('devices.show', [$customer->id, $device->id])}}">{{$device->name}}</a></td>
                <td>
                    @if(isset($device->upgradesets) && count($device->upgradesets) > 0)
                        Yes
                    @else
                        No
                    @endif
                </td>
                <td><a href="{{route('devices.edit', [$customer->id, $device->id])}}" class="pull-right">Edit</a></td>
            </tr>
        @endforeach
    </table>
    @endif
    </div>

@endsection