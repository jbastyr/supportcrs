@extends('layouts.app')
@section('content')
    <a href="{{route('customers.index')}}" class="btn btn-default">Go Back</a>
    <h1>New Customer</h1>
    {!! Form::open(['action' => 'CustomerController@store', 'method' => 'POST']) !!}
        <div class='form-group'>
            {{Form::label('name', 'Name')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name', 'autofocus'])}}
        </div>
        
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close()!!}
@endsection