@extends('layouts.app')
@section('content')
<a href="{{route('customers.index')}}" class="btn btn-default">Go Back</a>
    <h1>Edit Customer</h1>
    {!! Form::open(['action' => ['CustomerController@update', $customer->id], 'method' => 'POST']) !!}
        <div class='form-group'>
            {{Form::label('name', 'Name')}}
            {{Form::text('name', $customer->name, ['class' => 'form-control', 'placeholder' => 'Name', 'autofocus'])}}
        </div>
        {{Form::hidden('_method', 'PUT')}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close()!!}
    <hr>
    <div class="container">
        Please note that deleting a customer will remove all information associated with it
        {!! Form::open([
            'action' => ['CustomerController@destroy', $customer->id], 
            'method' => 'POST', 
            'class' => 'pull-right'
            ]) !!}
            {{Form::hidden('_method', 'DELETE')}}
            {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
        {!! Form::close() !!}
    </div>
@endsection