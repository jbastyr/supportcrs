@extends('layouts.app')
@section('content')
    <a href="{{route('apikeys.index')}}" class="btn btn-default">Go Back</a>
    <h1>New ApiKey</h1>
    {!! Form::open(['action' => 'ApiKeyController@store', 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::hidden('_user_id', Auth::user()->id )}}
            {{Form::submit('Generate Key', ['class' => 'btn btn-primary'])}}
        </div>
    {!! Form::close() !!}
@endsection