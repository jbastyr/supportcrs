@extends('layouts.app')
@section('content')
    <h1>
        Api Keys
        <a href="{{route('apikeys.create')}}" class="btn btn-primary pull-right">New Api Key</a>
    </h1>
    <?php 
        $user = Auth::user();
        $apikey = $user->apikey;
    ?>
    @if(isset($apikey))
        <div class="well">
            Api Key for {{$user->name}}: {{$apikey->key}}
        </div>
    @endif
@endsection