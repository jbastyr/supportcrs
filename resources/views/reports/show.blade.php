@extends('layouts.report')
@section('content')
    <div class="page-header, text-center" style="margin-top:10em;">
        <h1 style="font-weight:bold;font-size:3.25em;">{{$customer->name}}</h1>
        <div class="container">
            <h2>Annual Systems Assessment</h2>
            <p>Date of review: {{getdate()['mon'] . '/' . getdate()['mday'] . '/' . getdate()['year']}}</p>
            
            <p style="margin:7em 4em;font-size:1.5em;">You are receiving this asssessment as part of an ongoing effort to continually improve the efficiency, integrity, and functionality of the network and computers in your organization. After reviewing the current status of your machines, we believe these suggestions would translate to a measurable improvement in performance and also increase the lifespan of your systems<p>
        </div>
    </div>
    <br class="page-break" />
    
    @foreach($customer->devices as $device)
        @if(count($device->upgradesets) > 0)
        <img src="{{asset('img/test.png')}}" alt="spacing">
        <div class="container" style="margin-top:10em;">
            <h2>{{$device->name}}</h2>
            <table class="table">
                <thead>
                    <th class="text-center">Current Configuration</th>
                </thead>
                <tr class="text-center"><td>{{$device->os}}</td></tr>
                <tr class="text-center"><td>{{$device->ram}}GB RAM</td></tr>
                <tr class="text-center"><td>{{$device->cpu}}</td></tr>
            </table>

            <table class="table">
                <thead>
                    <th>Upgrades Recommended</th>
                    <th>Cost</th>
                </thead>
                <tbody>
                @foreach($device->upgradesets as $upgradeset)
                    <!-- hardware -->
                    <tr><th colspan="2">Hardware</th></tr>
                    @foreach($upgradeset->upgrades as $upgrade)
                        @if($upgrade->asset->type->type == 'Hardware')
                        <tr>
                            <td>{{$upgrade->asset->name}}</td>
                            <td>{{$upgrade->historical_cost}}</td>
                        </tr>
                        @endif
                    @endforeach

                    <!-- software -->
                    <tr><th colspan="2">Software</th></tr>
                    @foreach($upgradeset->upgrades as $upgrade)
                        @if($upgrade->asset->type->type == 'Software')
                        <tr>
                            <td>{{$upgrade->asset->name}}</td>
                            <td>{{$upgrade->historical_cost}}</td>
                        </tr>
                        @endif
                    @endforeach

                    <!-- labor -->
                    <tr><th colspan="2">Labor</th></tr>
                    @foreach($upgradeset->upgrades as $upgrade)
                        @if($upgrade->asset->type->type == 'Labor')
                        <tr>
                            <td>{{$upgrade->asset->name}}</td>
                            <td>{{$upgrade->historical_cost}}</td>
                        </tr>
                        @endif
                    @endforeach


                    <tr>
                        <td colspan=2>
                            @foreach((explode("\n", $upgradeset->comment)) as $commentpart)
                                {{$commentpart}}
                                <br />
                            @endforeach
                            
                        </td>
                    </tr>
                @endforeach
            </tbody>
            </table>
            <div>
                <p>{{$device->comment}}</p>
            </div>
        </div>
        <br class="page-break" />
        @endif
    @endforeach


    <!--- Justification for upgrades --->
    <img src="{{asset('img/test.png')}}" alt="spacing">
    <div class="container" style="margin-top:10em;">
        <h2>Upgrades 101</h2>
        <br />
        <h4>SSD Upgrade</h4>
        <p>Upgrading from a hard disk drive (HDD) to a solid state drive (SSD) sees on average a 10x speedup in general use and boot time. SSDs have no moving parts so they are not as prone to physical damage. As such, they have a substantially longer life span as well</p>
        <br />

        <h4>RAM Upgrade</h4>
        <p>Upgrading RAM helps in general use of the computer, especially evident with larger applications and multitasking such as working in Outlook, Word, Excel, CAD and CRM software, web browsers all at the same time. This is especially important for servers handling large workloads for many employees</p>
        <br />

        <h4>Software Upgrade</h4>
        <p>Microsoft and other companies regularly drop support for older versions of their software, including Windows and Office. After this point, vendor support and updates are not maintained or available, opening the door to possible security issues</p>
        <p>Current mainstream supported versions: Windows 8.1, 10, Server 2016, 2019, and Office 2016, 2019</p>
        <p>Extended support is still available for Windows 7, Server 2012, 2012 R2, and Office 2013</p>
        <br />

    </div>
    <br class="page-break" />

    <!-- Summary -->
    <img src="{{asset('img/test.png')}}" alt="spacing">
    <div class="container" style="margin-top:10em;">
        <h2>Summary</h2>
        <table class="table">
            <thead>
                <th>Computer</th>
                <th>Cost</th>
            </thead>
            <tbody>
                <?php $custTotal = 0; ?>
                @foreach($customer->devices as $device)
                    @if(count($device->upgradesets) > 0)
                        <tr>
                            <td>{{$device->name}}</td>
                            <td>
                                <?php
                                    $total = 0; 
                                    foreach($device->upgradesets as $upgradeset){
                                        if(isset($upgradeset->upgrades)){
                                            foreach($upgradeset->upgrades as $upgrade){
                                                $total += $upgrade->historical_cost;
                                            }
                                        }
                                    }
                                    $custTotal += $total;
                                    //$total = ceil($total);
                                    echo e($total);
                                ?>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
            <tfoot>    
                <tr style="border-top: solid 2px;">
                    <td><strong>Organization total (before tax)</strong></td>
                    <td><strong>{{$custTotal}}</strong></td>
                </tr>
            </tfoot>
        </table>
    </div>
    <div class="container" style="margin-top:10em;">

        <h4 style="margin:0em 4em;text-align:center;">To answer any questions and proceed with suggested upgrades, please contact us at the number below.</h4>
        
    </div>
@endsection