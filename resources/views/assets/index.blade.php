@extends('layouts.app')
@section('scripts')
<script>
    function selectAsset(){
        var assetName = document.getElementById("searchterm").value;
        var selector = 'option[value="' + assetName + '"]';
        var assetID = document.querySelector(selector).id;
        var targetURL = "{{route('assets.index')}}" + "/" + assetID;
        window.location.href = targetURL;
    }
</script>
@endsection
@section('content')
    <h1>
        Assets
        <a href="{{route('assets.create')}}" class="btn btn-primary pull-right">New Asset</a>
    </h1>
    @if(isset($assets) && count($assets) > 0)
        <table class="table table-hover">    
            <tr><th>Name</th><th>Cost</th><th></th></tr>
            @foreach($assets as $asset)
                <tr>
                    <td><a href="{{route('assets.show', $asset->id)}}">{{$asset->name}}</a></td>
                    <td>${{$asset->cost}}</td>
                    <td><a href="{{route('assets.edit', $asset->id)}}" class="pull-right">Edit</a></td>
                </tr>
            @endforeach
        </table>
        {{$assets->links()}}
    @endif

    <h3>Search</h3>
    <div class="input-group">
        <input type="text" id="searchterm" class="form-control pull-left" list="assets" placeholder="Asset">
        
        <datalist id="assets">
            @if(isset($allAssets) && count($allAssets) > 0)
                @foreach($allAssets as $asset)
                    <option value="{{$asset->name}}" id="{{$asset->id}}">
                @endforeach
            @endif
        </datalist>
        <span class="input-group-btn">
            <button class="btn btn-primary pull-right" type="button" id="search" name="search" onclick="selectAsset();">Select</button>
        </span>
    </div>
@endsection