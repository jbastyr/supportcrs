@extends('layouts.app')
@section('content')
    <a href="{{route('assets.index')}}" class="btn btn-default">Go Back</a>
    <h1>New Asset</h1>
    {!! Form::open(['action' => 'AssetController@store', 'method' => 'POST']) !!}
        <div class='form-group'>
            {{Form::label('name', 'Name')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name', 'autofocus'])}}
        </div>
        <div class='form-group'>
            {{Form::label('cost', 'Cost')}}
            {{Form::text('cost', '', ['class' => 'form-control', 'placeholder' => 'Cost in $'])}}
        </div>
        <div class='form-group'>
            {{Form::label('assettype_id', 'Asset Type')}}
            {{Form::select('assettype_id', ['1' => 'Hardware', '2' => 'Software', '3' => 'Labor'])}}
        </div>
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close()!!}
@endsection