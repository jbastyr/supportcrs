@extends('layouts.app')
@section('content')
    <a href="{{route('assets.index')}}" class="btn btn-default">Go Back</a>
    <h1>
        {{$asset->name}}
    </h1>
    <table class="table table-hover">    
        <tr>
            <th>Property</th>
            <th>Value</th>
        </tr>
        <tr>
            <td>Name</td>
            <td>{{$asset->name}}</td>
        </tr>
        <tr>
            <td>Cost</td>
            <td>${{$asset->cost}}</td>
        </tr>
        <tr>
            <td>Asset Type</td>
            <td>{{$asset->type->type}}</td>
        </tr>
    </table>
@endsection