@extends('layouts.app')
@section('content')
<a href="{{route('assets.index')}}" class="btn btn-default">Go Back</a>
    <h1>Edit Asset</h1>
    {!! Form::open(['action' => ['AssetController@update', $asset->id], 'method' => 'POST']) !!}
        <div class='form-group'>
            {{Form::label('name', 'Name')}}
            {{Form::text('name', $asset->name, ['class' => 'form-control', 'placeholder' => 'Name', 'autofocus'])}}
        </div>
        <div class='form-group'>
            {{Form::label('cost', 'Cost')}}
            {{Form::text('cost', $asset->cost, ['class' => 'form-control', 'placeholder' => 'Cost in $'])}}
        </div>
        <div class='form-group'>
            {{Form::label('assettype_id', 'Asset Type')}}
            {{Form::select('assettype_id', ['1' => 'Hardware', '2' => 'Software', '3' => 'Labor'], $asset->type->id)}}
        </div>
        {{Form::hidden('_method', 'PUT')}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close()!!}
    <hr>
    <div class="container">
        {!! Form::open([
            'action' => ['AssetController@destroy', $asset->id], 
            'method' => 'POST', 
            'class' => 'pull-right'
            ]) !!}
            {{Form::hidden('_method', 'DELETE')}}
            {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
        {!! Form::close() !!}
    </div>
@endsection