@extends('layouts.app')
@section('content')
<a href="{{route('customers.show', $customer_id)}}" class="btn btn-default">Go Back</a>
    <h1>New Password</h1>
    {!! Form::open(['action' => ['PasswordController@store', $customer_id], 'method' => 'POST']) !!}
        <div class='form-group'>
            {{Form::label('name', 'Name')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name', 'autofocus'])}}
        </div>
        <div class='form-group'>
            {{Form::label('login', 'Login')}}
            {{Form::text('login', '', ['class' => 'form-control', 'placeholder' => 'Login'])}}
        </div>
        <div class='form-group'>
            {{Form::label('password', 'Password')}}
            {{Form::text('password', '', ['class' => 'form-control', 'placeholder' => 'Password'])}}
        </div>
        {{Form::hidden('_customer_id', $customer_id)}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close()!!}
@endsection