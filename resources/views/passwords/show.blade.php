@extends('layouts.app')
@section('content')
<a href="{{route('customers.show', $customer->id)}}" class="btn btn-default">Go Back</a>
    @if(isset($password))    
    <h1>{{$customer->name . " | " . $password->name}}</h1>
    <table class="table table-hover">
        <tr>
            <th>Login</th>
            <th>Password</th>
            <th></th>
        </tr>
        <tr>
            <td>{{$password->login}}</td>
            <td>{{$password->password}}</td>
            <td><a href="{{route('passwords.edit', [$customer->id, $password->id])}}" class="btn btn-default pull-right">Edit</a></td>
        </tr>
    </table>
    @endif
@endsection