@extends('layouts.app')
@section('content')
<a href="{{route('customers.show', $customer->id)}}" class="btn btn-default">Go Back</a>
    <h1>Edit Password</h1>
    {!! Form::open(['action' => ['PasswordController@update', $customer->id, $password->id], 'method' => 'POST']) !!}
        <div class='form-group'>
            {{Form::label('name', 'Name')}}
            {{Form::text('name', $password->name, ['class' => 'form-control', 'placeholder' => 'Name', 'autofocus'])}}
        </div>
        <div class='form-group'>
            {{Form::label('login', 'Login')}}
            {{Form::text('login', $password->login, ['class' => 'form-control', 'placeholder' => 'Login'])}}
        </div>
        <div class='form-group'>
            {{Form::label('password', 'Password')}}
            {{Form::text('password', $password->password, ['class' => 'form-control', 'placeholder' => 'Password'])}}
        </div>
        {{Form::hidden('_method', 'PUT')}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close()!!}

    {!! Form::open([
        'action' => ['PasswordController@destroy', $customer->id, $password->id], 
        'method' => 'POST', 
        'class' => 'pull-right'
        ]) !!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
    {!! Form::close() !!}
@endsection