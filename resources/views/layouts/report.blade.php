<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token in all requests-->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'CRS') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        br.page-break {
            page-break-after: always;
        }
    </style>
    <!-- Scripts at top to prevent flash of unstyled content -->
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('scripts')
</head>
<body>
    <div id="report">
        @yield('content')
    </div>
</body>
</html>