@extends('layouts.app')
@section('content')
<a href="{{route('customers.show', $customer->id)}}" class="btn btn-default">Go Back</a>
    <h1>Edit Device</h1>
    {!! Form::open(['action' => ['DeviceController@update', $customer->id, $device->id], 'method' => 'POST']) !!}
        
        <div class="form-group">
            {{Form::label('name', 'Device Name')}}
            {{Form::text('name', $device->name, ['class' => 'form-control', 'placeholder' => 'Device Name', 'autofocus'])}}
        </div>
        <div class="form-group">
            {{Form::label('class', 'Device Class')}}
            {{Form::text('class', $device->class, ['class' => 'form-control', 'placeholder' => 'Device Class (Workstation, Laptop)', 'autofocus'])}}
        </div>
        <div class="form-group">
            {{Form::label('os', 'Operating System')}}
            {{Form::text('os', $device->os, ['class' => 'form-control', 'placeholder' => 'Operating System', 'autofocus'])}}
        </div>
        <div class="form-group">
            {{Form::label('ram', 'Installed Memory')}}
            {{Form::text('ram', $device->ram, ['class' => 'form-control', 'placeholder' => 'Installed Memory (8, 16 - No GB included)', 'autofocus'])}}
        </div>
        <div class="form-group">
            {{Form::label('cpu', 'Processor')}}
            {{Form::text('cpu', $device->cpu, ['class' => 'form-control', 'placeholder' => 'Processor (CPU)', 'autofocus'])}}
        </div>
        <div class="form-group">
            {{Form::label('hdd', 'Main HDD Size')}}
            {{Form::text('hdd', $device->hdd, ['class' => 'form-control', 'placeholder' => 'Main HDD Size (500, 1000 - No GB included)', 'autofocus'])}}
        </div>
        <div class="form-group">
            {{Form::label('apps', 'Applications')}}
            {{Form::text('apps', $device->apps, ['class' => 'form-control', 'placeholder' => 'Applications (QB, Office)', 'autofocus'])}}
        </div>


        {{Form::hidden('_method', 'PUT')}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close()!!}

    {!! Form::open([
        'action' => ['DeviceController@destroy', $customer->id, $device->id], 
        'method' => 'POST', 
        'class' => 'pull-right'
        ]) !!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
    {!! Form::close() !!}
@endsection