@extends('layouts.app')
@section('content')
<a href="{{route('customers.show', $customer->id)}}" class="btn btn-default">Go Back</a>
    @if(isset($device))    
    <h1>{{$customer->name . " | " . $device->name}}</h1>
    <div class="container">
        <table class="table table-hover">    
            <tr>
                <th>Property</th>
                <th>Value</th>
            </tr>
            <tr>
                <td>Class</td>
                <td>{{$device->class}}</td>
            </tr>
            <tr>
                <td>Operating System</td>
                <td>{{$device->os}}</td>
            </tr>
            <tr>
                <td>Installed Memory</td>
                <td>{{$device->ram}} GB</td>
            </tr>
            <tr>
                <td>Processor</td>
                <td>{{$device->cpu}}</td>
            </tr>
            <tr>
                <td>Main HDD Size</td>
                <td>{{$device->hdd}} GB</td>
            </tr>
            <tr>
                <td>Applications</td>
                <td>{{$device->apps}}</td>
            </tr>
        </table>
        <a href="{{route('devices.edit', [$customer->id, $device->id])}}" class="btn btn-default pull-right">Edit</a>
    </div>
    
    <div class="container">
        <h3>Upgrade Sets
            <a href="{{route('upgradesets.create', [$customer->id, $device->id])}}" class="btn btn-primary pull-right">New Upgrade Set</a>
        </h3>
        <table class="table table-hover">
            <thead>
                <th>Last Updated</th>
                <th></th>
            </thead>
            <tbody>
            @if(isset($upgradesets) && count($upgradesets) > 0)
            @foreach($upgradesets as $upgradeset)
                <tr>
                    <td><a href="{{route('upgradesets.show', [$customer->id, $device->id, $upgradeset->id])}}" >{{substr($upgradeset->updated_at, 0, 16)}}</td>
                    <td><a href="{{route('upgradesets.edit', [$customer->id, $device->id, $upgradeset->id])}}" class="btn btn-default pull-right">Edit</td>
                </tr>
            @endforeach
            @endif
            </tbody>
        </table>
            
    </div>



    @endif
@endsection