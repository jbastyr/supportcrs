@extends('layouts.app')
@section('scripts')
<script>
    function rewriteAssetID(){
        var assetName = document.getElementById("assetselect").value;
        var selector = 'option[value="' + assetName + '"]';
        var assetID = document.querySelector(selector).id;
        document.getElementsByName("asset_id")[0].value = assetID;
        document.getElementById("create-asset-form").submit();
    }
</script>
@endsection
@section('content')
    <a href="{{route('upgradesets.show', [$customer->id, $device->id, $upgradeset->id])}}" class="btn btn-default">Go Back</a>
    <h1>Edit Upgrade</h1>
    {!! Form::open(['id' => 'create-asset-form', 'action' => ['UpgradeController@update', $customer->id, $device->id, $upgradeset->id, $upgrade->id], 'method' => 'POST'])!!}
        <div class="form-group">
            {{Form::label('asset', 'Asset')}}
            {{Form::text('asset_name', $upgrade->asset->name, ['class' => 'form-control', 'id' => 'assetselect', 'list' => 'assets', 'autofocus'])}}
            <datalist id="assets">
                @if(isset($assets) && count($assets) > 0)
                    @foreach($assets as $asset)
                        <option value="{{$asset->name}}" id="{{$asset->id}}">
                    @endforeach
                @endif
            </datalist>
        </div>
        <div class="form-group">
            {{Form::label('amount', 'Amount')}}
            {{Form::number('amount', $upgrade->amount, ['class' => 'form-control'])}}
        </div>
        {{Form::hidden('asset_id', '')}}
        {{Form::hidden('_customer_id', $customer->id)}}
        {{Form::hidden('_device_id', $device->id)}}
        {{Form::hidden('_upgradeset_id', $upgradeset->id)}}

        {{Form::hidden('_method', 'PUT')}}
        <button class="btn btn-primary" type="button" onclick="rewriteAssetID();">Submit</button>
    {!! Form::close()!!}

    {!! Form::open([
        'action' => ['UpgradeController@destroy',
            $customer->id,
            $device->id,
            $upgradeset->id,
            $upgrade->id],
        'method' => 'POST',
        'class' => 'pull-right'
    ])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
    {!! Form::close() !!}
@endsection