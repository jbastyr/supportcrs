@extends('layouts.app')
@section('scripts')
<script>
    function rewriteAssetID(){
        var assetName = document.getElementById("assetselect").value;
        var selector = 'option[value="' + assetName + '"]';
        var assetID = document.querySelector(selector).id;
        document.getElementsByName("asset_id")[0].value = assetID;
        document.getElementById("create-upgrade-form").submit();
    }
</script>
@endsection
@section('content')
    <a href="{{route('devices.show', [$customer_id, $device_id])}}" class="btn btn-default">Go Back</a>
    <h1>New Upgrade</h1>
    {!! Form::open(['id' => 'create-upgrade-form', 'action' => ['UpgradeController@store', $customer_id, $device_id, $upgradeset_id], 'method' => 'POST'])!!}
        <div class="form-group">
            {{Form::label('asset', 'Asset')}}
            {{Form::text('asset_name', '', ['class' => 'form-control', 'id' => 'assetselect', 'list' => 'assets', 'autofocus'])}}
            <datalist id="assets">
                @if(isset($assets) && count($assets) > 0)
                    @foreach($assets as $asset)
                        <option value="{{$asset->name}}" id="{{$asset->id}}">
                    @endforeach
                @endif
            </datalist>
        </div>
        <div class="form-group">
            {{Form::label('amount', 'Amount')}}
            {{Form::number('amount', '1', ['class' => 'form-control'])}}
        </div>
        {{Form::hidden('asset_id', '')}}
        {{Form::hidden('_customer_id', $customer_id)}}
        {{Form::hidden('_device_id', $device_id)}}
        {{Form::hidden('_upgradeset_id', $upgradeset_id)}}
        <button class="btn btn-primary" type="button" onclick="rewriteAssetID();">Submit</button>
    {!! Form::close()!!}
@endsection