@extends('layouts.app')
@section('content')
<a href="{{route('upgradesets.show', [$customer->id, $device->id, $upgradeset->id])}}" class="btn btn-default">Go Back</a>
    @if(isset($upgrade))
    <h1>{{$customer->name . " | " . $device->name . " | " . substr($upgradeset->updated_at, 0, 16) . " | " . $upgrade->asset->name}}</h1>
    <div class="container">
        <table class="table table-hover">    
            <tr>
                <th>Property</th>
                <th>Value</th>
            </tr>
            <tr>
                <td>Upgrade</td>
                <td>{{$upgrade->asset->name}}</td>
            </tr>
            <tr>
                <td>Amount</td>
                <td>{{$upgrade->amount}}</td>
            </tr>
            <tr>
                <td>Cost</td>
                <td>{{$upgrade->historical_cost}} GB</td>
            </tr>

        </table>
        <a href="{{route('upgrades.edit', [$customer->id, $device->id, $upgradeset->id, $upgrade->id])}}" class="btn btn-default pull-right">Edit</a>
    </div>
@endif
@endsection