@extends('layouts.app')
@section('content')
    <a href="{{route('devices.show', [$customer_id, $device_id])}}" class="btn btn-default">Go Back</a>
    <h1>Edit UpgradeSet | {{App\Device::find($device_id)->name}}</h1>
    {!! Form::open(['action' => ['UpgradeSetController@update', $customer_id, $device_id, $upgradeset->id], 'method' => 'POST'])!!}
        <div class="form-group">
            {{Form::label('comment', 'Comment')}}
            {{Form::textarea('comment', $upgradeset->comment, ['class' => 'form-control', 'autofocus', 'rows' => 3])}}
        </div>

        {{Form::hidden('_device_id', $device_id)}}
        {{Form::hidden('_method', 'PUT')}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close()!!}

    {!! Form::open([
        'action' => ['UpgradeSetController@destroy',
            $customer_id,
            $device_id,
            $upgradeset->id],
        'method' => 'POST',
        'class' => 'pull-right'
    ])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
    {!! Form::close() !!}
@endsection