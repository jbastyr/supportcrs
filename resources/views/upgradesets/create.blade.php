@extends('layouts.app')
@section('content')
    <a href="{{route('devices.show', [$customer_id, $device_id])}}" class="btn btn-default">Go Back</a>
    <h1>New UpgradeSet | {{App\Device::find($device_id)->name}}</h1>
    {!! Form::open(['action' => ['UpgradeSetController@store', $customer_id, $device_id], 'method' => 'POST'])!!}
        <div class="form-group">
            {{Form::label('comment', 'Comment')}}
            {{Form::textarea('comment', '', ['class' => 'form-control', 'autofocus', 'rows' => 3])}}
        </div>
        {{Form::hidden('_device_id', $device_id)}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close()!!}
@endsection