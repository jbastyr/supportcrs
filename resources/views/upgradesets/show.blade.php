@extends('layouts.app')
@section('content')
<a href="{{route('devices.show', [$customer->id, $device->id])}}" class="btn btn-default">Go Back</a>
    @if(isset($upgradeset))
    <h1>{{$customer->name . " | " . $device->name . " | UpgradeSet"}}</h1>
    <div class="container">
        <table class="table table-hover">    
            <tr>
                <th>Upgrade</th>
                <th><a href="{{route('upgrades.create', [$customer->id, $device->id, $upgradeset->id])}}" class="btn btn-primary pull-right">New Upgrade</a></th>
            </tr>
            @if(isset($upgradeset->upgrades) && count($upgradeset->upgrades) > 0) 
            @foreach($upgradeset->upgrades as $upgrade)
            <tr>
                <td>
                    <a href="{{route('upgrades.show', [$customer->id, $device->id, $upgradeset->id, $upgrade->id])}}">
                        {{$upgrade->asset->name}}
                    </a>
                </td>
                <td>
                    <a href="{{route('upgrades.edit', [$customer->id, $device->id, $upgradeset->id, $upgrade->id])}}" class="btn btn-default pull-right">
                        Edit
                    </a>
                </td>
            </tr>
            @endforeach
            @endif
        </table>
        <h4>Comment</h4>
        <p>{{$upgradeset->comment}}</p>
        <a href="{{route('upgradesets.edit', [$customer->id, $device->id, $upgradeset->id])}}" class="btn btn-default pull-right">Edit</a>
    </div>
    @endif
@endsection