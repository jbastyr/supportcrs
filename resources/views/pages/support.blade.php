@extends('layouts.app')
@section('content')
    <div class="jumbotron text-center">
        <h1>Support</h1>
    <p>If you do not have a Support Code, please <a href="{{route('website')}}">Contact Us</a></p>
    <iframe name="ScreenConnect" width="525" height="390" align="middle" src="{{ config('app.scurl', '/') }}" frameborder="0" scrolling="no"></iframe>
    </div>
@endsection