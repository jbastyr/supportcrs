@extends('layouts.app')
@section('content')
<a href="{{route('customers.show', $customer->id)}}" class="btn btn-default">Go Back</a>
    <h1>Edit Upload</h1>
    {!! Form::open(['action' => ['UploadController@update', $customer->id, $upload->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class='form-group'>
            {{Form::label('display_name', 'Display Name')}}
            {{Form::text('display_name', $upload->display_name, ['class' => 'form-control', 'placeholder' => 'Display Name', 'autofocus'])}}
        </div>
        <div class='form-group'>
            <h4>Original: <a href="{{route('uploads.show', [$customer->id, $upload->id])}}">{{$upload->display_name}}</a></h4>
            {{Form::file('file_name')}}
        </div>
        {{Form::hidden('_method', 'PUT')}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close()!!}

    {!! Form::open([
        'action' => ['UploadController@destroy', $customer->id, $upload->id], 
        'method' => 'POST', 
        'class' => 'pull-right'
        ]) !!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
    {!! Form::close() !!}
@endsection