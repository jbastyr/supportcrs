@extends('layouts.app')
@section('content')
<a href="{{route('customers.show', $customer_id)}}" class="btn btn-default">Go Back</a>
    <h1>New Upload</h1>
    {!! Form::open(['action' => ['UploadController@store', $customer_id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class='form-group'>
            {{Form::label('display_name', 'Display Name')}}
            {{Form::text('display_name', '', ['class' => 'form-control', 'placeholder' => 'Display Name', 'autofocus'])}}
        </div>
        <div class='form-group'>
            {{Form::file('file_name')}}
        </div>
        {{Form::hidden('_customer_id', $customer_id)}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close()!!}
@endsection