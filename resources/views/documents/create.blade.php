@extends('layouts.app')
@section('content')
<a href="{{route('customers.show', $customer_id)}}" class="btn btn-default">Go Back</a>
    <h1>New Document</h1>
    {!! Form::open(['action' => ['DocumentController@store', $customer_id], 'method' => 'POST']) !!}
        <div class='form-group'>
            {{Form::label('title', 'Title')}}
            {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title', 'autofocus'])}}
        </div>
        <div class='form-group'>
            {{Form::label('body', 'Body')}}
            {{Form::textarea('body', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body'])}}
        </div>
        {{Form::hidden('_customer_id', $customer_id)}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close()!!}
    <!--Needs to run after the text area is rendered-->
    <script>CKEDITOR.replace('article-ckeditor');</script>
@endsection