@extends('layouts.app')
@section('content')
<a href="{{route('customers.show', $customer->id)}}" class="btn btn-default">Go Back</a>
    <h1>Edit Document</h1>
    {!! Form::open(['action' => ['DocumentController@update', $customer->id, $document->id], 'method' => 'POST']) !!}
        <div class='form-group'>
            {{Form::label('title', 'Title')}}
            {{Form::text('title', $document->title, ['class' => 'form-control', 'placeholder' => 'Title', 'autofocus'])}}
        </div>
        <div class='form-group'>
            {{Form::label('body', 'Body')}}
            {{Form::textarea('body', $document->body, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body'])}}
        {{Form::hidden('_method', 'PUT')}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close()!!}

    {!! Form::open([
        'action' => ['DocumentController@destroy', $customer->id, $document->id], 
        'method' => 'POST', 
        'class' => 'pull-right'
        ]) !!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
    {!! Form::close() !!}
    <!--Needs to run after the textarea is rendered-->
    <script>CKEDITOR.replace('article-ckeditor');</script>
@endsection