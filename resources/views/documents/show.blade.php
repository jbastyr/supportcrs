@extends('layouts.app')
@section('content')
<a href="{{route('customers.show', $customer->id)}}" class="btn btn-default">Go Back</a>
    @if(isset($document))    
    <h1>{{$customer->name . " | " . $document->title}}</h1>
    <div>
        {!!$document->body!!}
    </div>
    <a href="{{route('documents.edit', [$customer->id, $document->id])}}" class="btn btn-default pull-right">Edit</a>
    @endif
@endsection