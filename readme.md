## SupportCRS

Completed
- Customers
- Devices
- Documents
- Passwords
- Uploads
- Assets
- AssetTypes
- Upgrades
- Reports Simple (Not full CRUD)
- User Auth
- Direct download for Uploads
- UpgradeSets for upgrade grouping and control
- API key for users, direct download support updated with this for auth

Todo
- Better support  for multiple upgradesets per device, either creating separate entries on the report or choosing at report creation
- - Alt: Only allow one upgradeset per device
- Homepage update, some other content here. iframe login, news, something
- Bug: upgradeset edit goback -> upgradeset, not device

Setup
- Install dependencies, get laravel ready
- Correct the php modules, laravel will complain about the necessary ones at install
- Install wysiwyg editor ckeditor
- Install the LaravelCollective HTMLForms modules
- Remember to symlink storage for uploads
- Create data or import old sql dump