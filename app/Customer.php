<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function passwords(){
        return $this->hasMany('App\Password')->orderBy('name');
    }
    
    public function documents(){
        return $this->hasMany('App\Document')->orderBy('title');
    }

    public function uploads(){
        return $this->hasMany('App\Upload')->orderBy('display_name');
    }

    public function devices(){
        return $this->hasMany('App\Device')->orderBy('name');
    }
}