<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetType extends Model
{
    protected $table = 'assettypes';
    public $primaryKey = 'id';
    public $timestamps = true;

}