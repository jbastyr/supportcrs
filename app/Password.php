<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Password extends Model
{
    protected $table = 'passwords';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function customer(){
        return $this->belongsTo('App\Customer');
    }
}
