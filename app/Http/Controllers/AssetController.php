<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Asset;
use App\AssetType;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $assets = Asset::orderBy('assettype_id')->orderBy('name')->paginate(10);
        $allAssets = Asset::all();
        return view('assets.index', ['assets' => $assets, 'allAssets' => $allAssets]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('assets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:assets',
            'cost' => 'required',
            'assettype_id' => 'required'
        ]);

        $asset = new Asset;
        $asset->name = $request->input('name');
        $asset->cost = $request->input('cost');
        $asset->assettype_id = $request->input('assettype_id');
        $asset->save();

        return redirect(route('assets.index'))
            ->with('success', 'Added asset successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asset = Asset::find($id);
        return view('assets.show', ['asset' => $asset]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asset = Asset::find($id);
        return view('assets.edit', ['asset' => $asset]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'cost' => 'required',
            'assettype_id' => 'required'
        ]);

        $asset = Asset::find($id);
        $asset->name = $request->input('name');
        $asset->cost = $request->input('cost');
        $asset->assettype_id = $request->input('assettype_id');
        $asset->save();

        return redirect(route('assets.index'))
            ->with('success', 'Updated asset successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $asset = Asset::find($id);
        $asset->delete();
        return redirect(route('assets.index'))
            ->with('success', 'Deleted asset successfully');
    }
}
