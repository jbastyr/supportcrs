<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Upload;
use App\Customer;
use App\ApiKey;

class UploadController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except('direct');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('uploads.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($customer_id)
    {
        return view('uploads.create', ['customer_id' => $customer_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'display_name' => 'required', 
            'file_name' => 'required|max:100000',
            '_customer_id' => 'required'
        ]);

        if($request->hasFile('file_name')){
            $filenameWithExt = $request->file('file_name')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file_name')->getClientOriginalExtension();
            $filenameToStore = $filename . '_' . time() . '.' . $extension;
            $path = $request->file('file_name')->storeAs('public/uploads', $filenameToStore);
        }else{
            return redirect(route('customers.show',
            $request->input('_customer_id')))
            ->with('error', 'No file selected');
        }

        $upload = new Upload;
        $upload->display_name = $request->input('display_name');
        $upload->original_name = $filenameWithExt;
        $upload->file_name = $filenameToStore;
        $upload->customer_id = $request->input('_customer_id');
        $upload->save();

        return redirect(route('customers.show',
        $request->input('_customer_id')))
        ->with('success', 'Added upload successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($customer_id, $id)
    {
        $upload = Upload::find($id);
        if ($upload->customer_id != $customer_id){
            return redirect(route('customers.show', $customer_id))->with('error', 'Upload does not belong to Customer');
        }
        //$customer = Customer::find($customer_id);
        return response()->download('storage/uploads/' . $upload->file_name, $upload->original_name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($customer_id, $id)
    {
        $upload = Upload::find($id);
        if ($upload->customer_id != $customer_id){
            return redirect(route('customers.show', $customer_id))->with('error', 'Upload does not belong to Customer');
        }
        $customer = Customer::find($customer_id);
        return view('uploads.edit', ['customer' => $customer, 'upload' => $upload]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $customer_id, $id)
    {
        $this->validate($request, [
            'display_name' => 'required', 
            'file_name' => 'nullable|max:100000'
        ]);

        $upload = Upload::find($id);
        if ($upload->customer_id != $customer_id){
            return redirect(route('customers.show', $customer_id))->with('error', 'Upload does not belong to Customer');
        }

        $upload->display_name = $request->input('display_name');

        if($request->hasFile('file_name')){
            $filenameWithExt = $request->file('file_name')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file_name')->getClientOriginalExtension();
            $filenameToStore = $filename . '_' . time() . '.' . $extension;
            $path = $request->file('file_name')->storeAs('public/uploads', $filenameToStore);

            //Remove old file from disk, we haven't changed the stored name yet
            Storage::delete('public/uploads/' . $upload->file_name);

            $upload->original_name = $filenameWithExt;
            $upload->file_name = $filenameToStore;
        }
        //Don't need to worry about changing the original and file _name if we
        // are not passing in a new one, just save
        $upload->save();

        return redirect(route('customers.show', $customer_id))
            ->with('success', 'Updated upload successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($customer_id, $id)
    {
        $upload = Upload::find($id);
        if ($upload->customer_id != $customer_id){
            return redirect(route('customers.show', $customer_id))->with('error', 'Upload does not belong to Customer');
        }

        //Remove file from disk, then remove row from database
        Storage::delete('public/uploads/' . $upload->file_name);
        $upload->delete();
        
        return redirect(route('customers.show', $customer_id))
            ->with('success', 'Deleted upload successfully');
    }


    // basic api key search, could be added per download but right now per user
    public function direct($id, $apikey){
        $upload = Upload::find($id);
        if(!isset($upload)){
            return;
        }
        $key = ApiKey::where('key', $apikey)->first();
        if(!isset($key)){
            return;
        }
        $customer_id = $upload->customer_id;

        return $this->show($customer_id, $id);
    }
}
