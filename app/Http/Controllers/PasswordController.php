<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Password;
use App\Customer;

class PasswordController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('passwords.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($customer_id)
    {
        return view('passwords.create', ['customer_id' => $customer_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required',
            'login' => 'required',
            '_customer_id' => 'required'
        ]);

        $password = new Password;
        $password->name = $request->input('name');
        $password->password = encrypt($request->input('password'));
        $password->customer_id = $request->input('_customer_id');
        //if($request->input('login') !== null ){
        $password->login = $request->input('login');
        //}
        $password->save();

        return redirect(route('customers.show', 
            $request->input('_customer_id')))
            ->with('success', 'Added password successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($customer_id, $id)
    {
        $password = Password::find($id);
        if ($password->customer_id != $customer_id){
            return redirect(route('customers.show', $customer_id))->with('error', 'Password does not belong to Customer');
        }
        $customer = Customer::find($customer_id);
        $password->password = decrypt($password->password);
        return view('passwords.show', ['customer' => $customer, 'password' => $password]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($customer_id, $id)
    {
        $password = Password::find($id);
        if ($password->customer_id != $customer_id){
            return redirect(route('customers.show', $customer_id))->with('error', 'Password does not belong to Customer');
        }
        $customer = Customer::find($customer_id);
        $password->password = decrypt($password->password);
        return view('passwords.edit', ['customer' => $customer, 'password' => $password]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $customer_id, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'login' => 'required',
            'password' => 'required'
        ]);

        $password = Password::find($id);
        if ($password->customer_id != $customer_id){
            return redirect(route('customers.show', $customer_id))->with('error', 'Password does not belong to Customer');
        }
        $password->name = $request->input('name');
        $password->password = encrypt($request->input('password'));
        $password->login = $request->input('login');
        $password->save();

        return redirect(route('customers.show', $customer_id))
            ->with('success', 'Updated password successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($customer_id, $id)
    {
        $password = Password::find($id);
        if ($password->customer_id != $customer_id){
            return redirect(route('customers.show', $customer_id))->with('error', 'Password does not belong to Customer');
        }
        $password->delete();
        return redirect(route('customers.show', $customer_id))
        ->with('success', 'Deleted password successfully');
    }
}