<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\Customer;

class DeviceController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('devices.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($customer_id)
    {
        return view('devices.create', ['customer_id' => $customer_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'class' => 'required',
            'os' => 'required',
            'ram' => 'required',
            'cpu' => 'required',
            'hdd' => 'required',
            'apps' => 'required',
            '_customer_id' => 'required'
        ]);

        $device = new Device;
        $device->name = $request->input('name');
        $device->class = $request->input('class');
        $device->os = $request->input('os');
        $device->ram = $request->input('ram');
        $device->cpu = $request->input('cpu');
        $device->hdd = $request->input('hdd');
        $device->apps = $request->input('apps');
        $device->customer_id = $request->input('_customer_id');
        $device->save();

        return redirect(route('customers.show', 
            $request->input('_customer_id')))
            ->with('success', 'Added device successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($customer_id, $id)
    {
        $device = Device::find($id);
        if ($device->customer_id != $customer_id){
            return redirect(route('customers.show', $customer_id))->with('error', 'Device does not belong to Customer');
        }
        $customer = Customer::find($customer_id);
        $upgradesets = $device->upgradesets;
        return view('devices.show', ['customer' => $customer, 'device' => $device, 'upgradesets' => $upgradesets]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($customer_id, $id)
    {
        $device = Device::find($id);
        if ($device->customer_id != $customer_id){
            return redirect(route('customers.show', $customer_id))->with('error', 'Device does not belong to Customer');
        }
        $customer = Customer::find($customer_id);
        return view('devices.edit', ['customer' => $customer, 'device' => $device]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $customer_id, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'class' => 'required',
            'os' => 'required',
            'ram' => 'required',
            'cpu' => 'required',
            'hdd' => 'required',
            'apps' => 'required',
        ]);

        $device = Device::find($id);
        if ($device->customer_id != $customer_id){
            return redirect(route('customers.show', $customer_id))->with('error', 'Device does not belong to Customer');
        }
        $device->name = $request->input('name');
        $device->class = $request->input('class');
        $device->os = $request->input('os');
        $device->ram = $request->input('ram');
        $device->cpu = $request->input('cpu');
        $device->hdd = $request->input('hdd');
        $device->apps = $request->input('apps');
        $device->save();

        return redirect(route('devices.show', [$customer_id, $device->id]))
            ->with('success', 'Updated device successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($customer_id, $id)
    {
        $device = Device::find($id);
        if ($device->customer_id != $customer_id){
            return redirect(route('customers.show', $customer_id))->with('error', 'Device does not belong to Customer');
        }
        $device->delete();
        return redirect(route('customers.show', $customer_id))
        ->with('success', 'Deleted device successfully');
    }
}
