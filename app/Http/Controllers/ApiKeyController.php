<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ApiKey;

class ApiKeyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('apikey.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('apikey.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            '_user_id' => 'required'
        ]);

        $apikey = new ApiKey;
        $apikey->user_id = $request->input('_user_id');
        $apikey->key = ApiKey::generateKey();
        $apikey->save();

        return redirect(route('index'))->with('success', 'Added apikey successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $apikey = ApiKey::find($id);
        return view('apikey.show', ['apikey' => $apikey]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $apikey = ApiKey::find($id);
        return view('apikey.edit', ['apikey' => $apikey]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            '_user_id' => 'required'
        ]);

        $apikey = ApiKey::find($id);
        $apikey->user_id = $request->input('_user_id');
        $apikey->key = ApiKey::generateKey();
        $apikey->save();

        return redirect(route('index'))
            ->with('success', 'Updated apikey successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $apikey = ApiKey::find($id);
        $apikey->delete();
        return redirect(route('index'))
            ->with('success', 'Deleted apikey successfully');
    }
}
