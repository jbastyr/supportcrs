<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Document;
use App\Customer;

class DocumentController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('documents.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($customer_id)
    {
        return view('documents.create', ['customer_id' => $customer_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            '_customer_id' => 'required'
        ]);

        $document = new Document;
        $document->title = $request->input('title');
        $document->body = $request->input('body');
        $document->customer_id = $request->input('_customer_id');
        $document->save();

        return redirect(route('customers.show', 
            $request->input('_customer_id')))
            ->with('success', 'Added document successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($customer_id, $id)
    {
        $document = Document::find($id);
        if ($document->customer_id != $customer_id){
            return redirect(route('customers.show', $customer_id))->with('error', 'Document does not belong to Customer');
        }
        $customer = Customer::find($customer_id);
        return view('documents.show', ['customer' => $customer, 'document' => $document]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($customer_id, $id)
    {
        $document = Document::find($id);
        if ($document->customer_id != $customer_id){
            return redirect(route('customers.show', $customer_id))->with('error', 'Document does not belong to Customer');
        }
        $customer = Customer::find($customer_id);
        return view('documents.edit', ['customer' => $customer, 'document' => $document]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $customer_id, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        $document = Document::find($id);
        if ($document->customer_id != $customer_id){
            return redirect(route('customers.show', $customer_id))->with('error', 'Document does not belong to Customer');
        }
        $document->title = $request->input('title');
        $document->body = $request->input('body');
        $document->save();

        return redirect(route('customers.show', $customer_id))
            ->with('success', 'Updated document successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($customer_id, $id)
    {
        $document = Document::find($id);
        if ($document->customer_id != $customer_id){
            return redirect(route('customers.show', $customer_id))->with('error', 'Document does not belong to Customer');
        }
        $document->delete();
        return redirect(route('customers.show', $customer_id))
        ->with('success', 'Deleted document successfully');
    }
}