<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Device;
use App\UpgradeSet;

class UpgradeSetController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('upgradesets.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($customer_id, $device_id)
    {
        return view('upgradesets.create', [
            'customer_id' => $customer_id,
            'device_id' => $device_id
            ]);
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            '_device_id' => 'required'
        ]);

        $upgradeset = new UpgradeSet;
        $upgradeset->device_id = $request->input('_device_id');

        if($request->input('comment') !== null){
            $upgradeset->comment = $request->input('comment');
        }else{
            $upgradeset->comment = "";
        }

        $upgradeset->save();

        // show the new set so we can add upgrades
        return redirect(route('upgradesets.show', [
            Device::find($request->input('_device_id'))->customer->id,
            $request->input('_device_id'),
            $upgradeset->id
        ]))->with('success', 'Added upgradeset successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($customer_id, $device_id, $id)
    {
        $customer = Customer::find($customer_id);
        $device = Device::find($device_id);
        $upgradeset = UpgradeSet::find($id);
        

        return view('upgradesets.show', [
            'customer' => $customer,
            'device' => $device,
            'upgradeset' => $upgradeset
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($customer_id, $device_id, $id)
    {
        $upgradeset = UpgradeSet::find($id);

        return view('upgradesets.edit', [
            'customer_id' => $customer_id,
            'device_id' => $device_id,
            'upgradeset' => $upgradeset
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $customer_id, $device_id, $id)
    {
        $this->validate($request, [
            '_device_id' => 'required'
        ]);
        $upgradeset = UpgradeSet::find($id);

        if($request->input('comment') !== null){
            $upgradeset->comment = $request->input('comment');
        }else{
            $upgradeset->comment = "";
        }
        
        $upgradeset->save();

        return redirect(route('upgradesets.show', [
            Device::find($request->input('_device_id'))->customer->id,
            $request->input('_device_id'),
            $upgradeset->id
        ]))->with('Success', 'Updated upgradeset successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($customer_id, $device_id, $id)
    {
        $upgradeset = UpgradeSet::find($id);
        $upgradeset->delete();
        return redirect(route('devices.show', [
            $customer_id,
            $device_id
        ]))->with('success', 'Deleted upgradeset successfully');
    }
}
