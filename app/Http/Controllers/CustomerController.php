<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Customer;
use App\Document;
use App\Password;
use App\Upload;
use App\Device;

class CustomerController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::orderBy('name')->paginate(10);
        $allCustomers = Customer::all(['id', 'name']);
        return view('customers.index', ['customers' => $customers, 'allCustomers' => $allCustomers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:customers'
        ]);

        $customer = new Customer;
        $customer->name = $request->input('name');
        $customer->save();

        return redirect(route('customers.index'))->with('success', 'Added customer successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::find($id);
        return view('customers.show', 
            ['customer' => $customer, 
            'passwords' => $customer->passwords, 
            'documents' => $customer->documents, 
            'uploads' => $customer->uploads,
            'devices' => $customer->devices]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        return view('customers.edit', ['customer' => $customer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        
        $customer = Customer::find($id);
        $customer->name = $request->input('name');
        $customer->save();

        return redirect(route('customers.index'))->with('success', 'Updated customer successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        foreach ($customer->uploads as $upload){
            Storage::delete('public/uploads/' . $upload->file_name);
        }
        $customer->delete();
        return redirect(route('customers.index'))->with('success', 'Deleted customer successfully');
    }


    //public function buildReport

    public function report($id){
        $customer = Customer::find($id);

        return view('reports.show', ['customer' => $customer]);
    }
}
