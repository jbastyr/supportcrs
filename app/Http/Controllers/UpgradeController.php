<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Upgrade;
use App\Asset;
use App\Customer;
use App\Device;
use App\UpgradeSet;

class UpgradeController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('upgrades.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($customer_id, $device_id, $upgradeset_id)
    {
        $assets = Asset::all(['id', 'name']);
        return view('upgrades.create', ['customer_id' => $customer_id, 'device_id' => $device_id, 'upgradeset_id' => $upgradeset_id, 'assets' => $assets]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'asset_id' => 'required',
            'amount' => 'required',
            '_customer_id' => 'required',
            '_device_id' => 'required',
            '_upgradeset_id' => 'required'
        ]);
        
        $upgrade = new Upgrade;
        $asset = Asset::find($request->input('asset_id'));

        $upgrade->asset_id = $request->input('asset_id');
        $upgrade->historical_cost = $asset->cost;
        $upgrade->amount = $request->input('amount');
        $upgrade->upgradeset_id = $request->input('_upgradeset_id');
        $upgrade->save();

        return redirect(route('upgradesets.show', [
            $request->input('_customer_id'), 
            $request->input('_device_id'),
            $request->input('_upgradeset_id')]
            ))->with('success', 'Added upgrade successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($customer_id, $device_id, $upgradeset_id, $id)
    {
        $upgrade = Upgrade::find($id);
        if($upgrade->upgradeset_id != $upgradeset_id){
            return redirect(route('upgradesets.show', [
                $customer_id, $device_id, $upgradeset_id
            ]))->with('error', 'Upgrade does not belong to upgradeset');
        }
        $customer = Customer::find($customer_id);
        $device = Device::find($device_id);
        $upgradeset = UpgradeSet::find($upgradeset_id);
        return view('upgrades.show', [
            'customer' => $customer,
            'device' => $device,
            'upgrade' => $upgrade,
            'upgradeset' => $upgradeset
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($customer_id, $device_id, $upgradeset_id, $id)
    {
        $upgrade = Upgrade::find($id);

        if($upgrade->upgradeset_id != $upgradeset_id){
            return redirect(route('upgradesets.show', [
                $customer_id, $device_id, $upgradeset_id
            ]))->with('error', 'Upgrade does not belong to upgradeset');
        }

        $assets = Asset::all(['id', 'name']);

        $customer = Customer::find($customer_id);
        $device = Device::find($device_id);
        $upgradeset = UpgradeSet::find($upgradeset_id);
        return view('upgrades.edit', [
            'customer' => $customer,
            'device' => $device,
            'upgrade' => $upgrade,
            'upgradeset' => $upgradeset,
            'assets' => $assets
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $customer_id, $device_id, $upgradeset_id, $id)
    {
        $this->validate($request, [
            'asset_id' => 'required',
            'amount' => 'required',
            '_customer_id' => 'required',
            '_device_id' => 'required',
            '_upgradeset_id' => 'required'
        ]);
        
        $upgrade = Upgrade::find($id);

        if($upgrade->upgradeset_id != $upgradeset_id){
            return redirect(route('upgradesets.show', [
                $customer_id, $device_id, $upgradeset_id
                ]))->with('error', 'Upgrade does not belong to upgradeset');
        }
        $asset = Asset::find($request->input('asset_id'));

        $upgrade->asset_id = $request->input('asset_id');
        $upgrade->historical_cost = $asset->cost;
        $upgrade->amount = $request->input('amount');
        $upgrade->upgradeset_id = $request->input('_upgradeset_id');
        $upgrade->save();

        return redirect(route('upgradesets.show', [
            $request->input('_customer_id'), 
            $request->input('_device_id'),
            $request->input('_upgradeset_id')]
            ))->with('success', 'Updated upgrade successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($customer_id, $device_id, $upgradeset_id, $id)
    {
        $upgrade = Upgrade::find($id);
        if($upgrade->upgradeset_id != $upgradeset_id){
            return redirect(route('upgradesets.show', [
                $customer_id, $device_id, $upgradeset_id
            ]))->with('error', 'Upgrade does not belong to upgradeset');
        }
        $upgrade->delete();
        return redirect(route('upgradesets.show', [
            $customer_id, $device_id, $upgradeset_id
            ]))->with('success', 'Deleted upgrade successfully');
    }
}