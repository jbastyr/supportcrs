<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Customer;
use App\Device;

class PageController extends Controller
{
    public function index(){
        return view('pages.index');
    }
    public function services(){
        return view('pages.services');
    }
    public function support(){
        return view('pages.support');
    }
    public function about(){
        return view('pages.about');
    }
    public function contact(){
        return view('pages.contact');
    }
   
    /*
   
    public function importdevices(){
        return $this->importFromFile();
        
        //return view('pages.importdevices');
    }

    public function importFromFile(){
        $allCustomers = Customer::all(['id', 'name']);
        $customersByName = $this->arrValues($allCustomers);
        //return $customersByName;
       
        $strRet = "";
        $handle = fopen("C:/Users/User/Desktop/AllDevices.csv", "r");
        $allCustomers = Customer::all(['id', 'name']);
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $properties = explode(",", $line);
                if(!$this->contains($customersByName, $properties[0])){
                    //$strRet .= $properties[0] . "<br>";
                    continue;
                }
                //continue;
                $properties[0] = $this->keyFromValue($allCustomers, $properties[0]);
                $this->importDevice($properties);
            }
            fclose($handle);
        } else {
            return var_dump($handle);
        } 



        return $strRet;
    }

    public function arrValues($entries){
        $arrayvalues = array();
        $keys = $entries->keys();
        for ($i = 0; $i < sizeof($keys); $i++){
            $arrayvalues[] = $entries->get($keys[$i])->name;
        }
        return $arrayvalues;
    }

    public function keyFromValue($array, $value){
        for($i = 0; $i < sizeof($array); $i++){
            if($array[$i]->name == $value){
                return $array[$i]->id;
            }
        }
    }

    public function importDevice($properties){
        $device = new Device;
        $device->name = $properties[1];
        $device->class = $properties[2];
        $device->os = $properties[3];
        $device->ram = $properties[4];
        $device->cpu = $properties[5];
        $device->hdd = $properties[6];
        $device->apps = $properties[7];
        $device->customer_id = $properties[0];
        $device->save();
    }

    public function contains($array, $value){
        foreach ($array as $entry){
            if ($entry == $value) {
                return true;
            }
        }
        return false;
    }

    */
    
}
