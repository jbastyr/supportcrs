<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    protected $table = 'assets';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function type(){
        return $this->belongsTo('App\AssetType', 'assettype_id');
    }
}