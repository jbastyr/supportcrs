<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upgrade extends Model
{
    protected $table = 'upgrades';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function upgradeset(){
        return $this->belongsTo('App\UpgradeSet', 'upgradeset_id');
    }

    public function asset(){
        return $this->belongsTo('App\Asset', 'asset_id');
    }
}