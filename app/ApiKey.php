<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiKey extends Model
{
    protected $table = 'apikey';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public static function generateKey(){
        //good enough for the use here
        return str_random(64);
    }
}