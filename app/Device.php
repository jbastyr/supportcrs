<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table = 'devices';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function customer(){
        return $this->belongsTo('App\Customer');
    }

    public function upgradesets(){
        return $this->hasMany('App\UpgradeSet');
    }
}
