<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpgradeSet extends Model
{
    protected $table = 'upgradesets';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function device(){
        return $this->belongsTo('App\Device', 'device_id');
    }

    public function upgrades(){
        return $this->hasMany('App\Upgrade', 'upgradeset_id');
    }
}