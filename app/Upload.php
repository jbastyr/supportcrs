<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $table = 'uploads';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function customer(){
        return $this->belongsTo('App\Customer');
    }
}
